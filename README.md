
# Ranking Super Smash Bros Ultimate for Brest (France) Smash community

## Comment fonctionne le ranking

### Critières de classement des joueurs dans le ranking

**Les joueurs ayant fait un bon classement remportent des points.**

Ces points sont attribués de façon plus ou moins élevée selon le classement du joueur et de la complexité du tournois.

La complexité du tournois et déterminées selon deux critières:

-Le nombre de participants présents dans le tournois.

-Le nombre de participants forts présents dans le tournois.


**Les joueurs constants sont favorisés.**

En effet, si un joueur 1 supposé fort se fait battre par un joueur 2 supposé moins fort, alors le joueur 1 perd des points et le joueur 2 en gagne.

Ces points sont attribués de façon plus ou moins élevés selon l'écart théorique de niveau des deux joueurs.

L'algorithme récompense les joueurs qui accomplissent des prouesses et encourage les joueurs qui vont l'avant pour avancer dans le ranking.


**La participation des joueurs**

Les joueurs qui participent à beaucoup de tournois sont plus favorisés car ils accumulent des points.


-----------------------------------------------------------------------------------------------------------------------------------------------------------------


# English translation

## How the Ranking Works

### Criteria for Ranking Players in the Ranking

**Players earn points based on their tournament performance.**

These points are awarded with varying values depending on the player's placement and the tournament's complexity.

The complexity of the tournament is determined by two criteria:

-The number of participants in the tournament.

-The number of strong participants in the tournament.


**Consistent players are favored.**

If a strong player 1 is defeated by a supposedly weaker player 2, player 1 loses points, and player 2 gains points. The points awarded are influenced by the theoretical skill gap between the two players.

The algorithm rewards players who achieve remarkable feats and rewards players to always strive forward in the ranking.


**Player Participation**

Players who participate in numerous tournaments are favored as they accumulate more points.