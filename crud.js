// crud.js
const db = require('./db');

// CREATE
async function createUser(pseudo, winrate, point, personnage, structure) {
  const [result] = await db.query('INSERT INTO joueurs (pseudoJoueur, winrateJoueur, pointJoueur, personnageJoueur, structureJoueur) VALUES (?, ?, ?, ?, ?)', [pseudo, winrate, point, personnage, structure]);
  return result.insertId;
}

// READ
async function getUsers() {
  const [rows] = await db.query('SELECT * FROM joueurs');
  return rows;
}

// UPDATE
async function updateUser(userId, newPseudo) {
  const [result] = await db.query('UPDATE joueurs SET pseudoJoueur = ? WHERE id = ?', [newPseudo, userId]);
  return result.affectedRows > 0;
}

// DELETE
async function deleteUser(userId) {
  const [result] = await db.query('DELETE FROM joueurs WHERE idJoueur = ?', [userId]);
  return result.affectedRows > 0;
}

async function getUserByName(userName) {
    const [rows] = await db.query('SELECT * FROM joueurs WHERE PseudoJoueur LIKE ?', [userName]);
    return rows;
  }

  async function addPlayer(userName) {
    const [rows] = await db.query('INSERT INTO joueurs (PseudoJoueur) VALUES (?)', [userName]);
    return rows;
  }

  async function updatePoint(nombrePoint, userId) {
    const [rows] = await db.query('UPDATE joueurs SET PointsJoueur = ? WHERE idJoueur = ?', [nombrePoint, userId]);
    return rows;
  }


module.exports = {
  createUser,
  getUsers,
  updateUser,
  deleteUser,
  getUserByName,
  addPlayer,
  updatePoint
};
