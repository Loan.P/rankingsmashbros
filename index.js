require('dotenv').config();

const { createUser, getUsers, updateUser, deleteUser, getUserByName, addPlayer, updatePoint } = require('./crud');

const fetch = (...args) => import('node-fetch').then(({ default: fetch }) => fetch(...args));
const startggURL = "https://api.start.gg/gql/alpha";
const startggkey = process.env.STARTGG_KEY;

const getEventId = async (tournamentName, eventName) => {
    const eventslug = `tournament/${tournamentName}/event/${eventName}`;
    let eventId;
    await fetch(startggURL, {
        method: 'POST',
        headers: {
            'content-type': 'application/json',
            'Accept': 'application/json',
            Authorization: 'Bearer ' + startggkey
        },
        body: JSON.stringify({
            query: "query EventQuery($slug:String) {event(slug: $slug) {id name}}",
            variables: {
                slug: eventslug
            },
        })
    }).then(r => r.json())
        .then(data => {
            console.log(data.data);
            eventId = data.data.event.id;
        });
    return eventId;
}



const getEventStandings = async (eventId, page, perPage) => {
    const eventslug = `eventId/${eventId}/page/${page}/perPage/${perPage}`;
    let placement;
    let idPlayer;
    let namePlayer;

    try {
        const response = await fetch(startggURL, {
            method: 'POST',
            headers: {
                'content-type': 'application/json',
                'Accept': 'application/json',
                Authorization: 'Bearer ' + startggkey
            },
            body: JSON.stringify({
                query: `
                    query CustomEventStandings($eventId: ID!, $page: Int!, $perPage: Int!) {
                        event(id: $eventId) {
                            standings(query: {
                                perPage: $perPage,
                                page: $page
                            }) {
                                nodes {
                                    placement
                                    entrant {
                                        id
                                        name
                                    }
                                }
                            }
                        }
                    }
                `,
                variables: {
                    eventId,
                    page,
                    perPage
                },
            }),
        });

        const data = await response.json();
        const standingsNode = data.data.event.standings.nodes[0];//Renvoit le top 1

        //console.log("Tous les participants" + JSON.stringify(data.data));


        if (standingsNode) {
            placement = standingsNode.placement;
            idPlayer = standingsNode.entrant.id;
            namePlayer = standingsNode.entrant.name;
        }
        return data.data;
    } catch (error) {
        console.error("Error fetching event standings:", error);
    }
}



const getEventSets = async (eventId, page, perPage) => {
  try {
      const response = await fetch(startggURL, {
          method: 'POST',
          headers: {
              'content-type': 'application/json',
              'Accept': 'application/json',
              Authorization: 'Bearer ' + startggkey
          },
          body: JSON.stringify({
              query: `
                  query EventSets($eventId: ID!, $page: Int!, $perPage: Int!) {
                      event(id: $eventId) {
                          id
                          name
                          sets(
                              page: $page
                              perPage: $perPage
                              sortType: STANDARD
                          ) {
                              pageInfo {
                                  total
                              }
                              nodes {
                                  id
                                  slots {
                                      id
                                      entrant {
                                          id
                                          name
                                      }
                                  }
                              }
                          }
                      }
                  }
              `,
              variables: {
                  eventId,
                  page,
                  perPage
              },
          }),
      });

      const data = await response.json();
      return data.data;
  } catch (error) {
      console.error("Error fetching event sets:", error);
  }
}

const getPhaseSeeds = async (phaseId, page, perPage) => {
  try {
      const response = await fetch(startggURL, {
          method: 'POST',
          headers: {
              'content-type': 'application/json',
              'Accept': 'application/json',
              Authorization: 'Bearer ' + startggkey
          },
          body: JSON.stringify({
              query: `
                  query PhaseSeeds($phaseId: ID!, $page: Int!, $perPage: Int!) {
                      phase(id: $phaseId) {
                          id
                          seeds(query: {
                              page: $page
                              perPage: $perPage
                          }) {
                              pageInfo {
                                  total
                                  totalPages
                              }
                              nodes {
                                  id
                                  seedNum
                                  entrant {
                                      id
                                      participants {
                                          id
                                          gamerTag
                                      }
                                  }
                              }
                          }
                      }
                  }
              `,
              variables: {
                  phaseId,
                  page,
                  perPage
              },
          }),
      });

      const data = await response.json();
      return data.data;
  } catch (error) {
      console.error("Error fetching phase seeds:", error);
  }
}



const getSetInfo = async (setId) => {
    try {
      const response = await fetch(startggURL, {
        method: 'POST',
        headers: {
          'content-type': 'application/json',
          'Accept': 'application/json',
          Authorization: 'Bearer ' + startggkey
        },
        body: JSON.stringify({
          query: `
            query SetInfo($setId: ID!) {
              set(id: $setId) {
                id
                slots {
                  id
                  standing {
                    id
                    placement
                    stats {
                      score {
                        label
                        value
                      }
                    }
                  }
                }
              }
            }
          `,
          variables: {
            setId,
          },
        }),
      });
  
      const data = await response.json();
      return data.data;
    } catch (error) {
      console.error("Error fetching set info:", error);
    }
  };





async function main() {


  let tournois = [
    ["last-player-ultimate-saison-2023-2024-1","double-limination", 0, 1453029],//nom tournoi, nom Bracket, id Tournoi, id Bracket
    ["last-player-ultimate-saison-2023-2024-2","double-limination", 0, 1459617],
    ["last-player-ultimate-saison-2023-2024-3","double-limination", 0, 1471243],
    ["last-player-ultimate-saison-2023-2024-4","double-limination", 0, 1489180],
    ["last-player-ultimate-saison-2023-2024-5","double-limination", 0, 1494757],
    ["last-player-ultimate-saison-2023-2024-6","double-limination", 0, 1500276],
    ["last-player-ultimate-saison-2023-2024-7","double-limination", 0, 1505988],
    ["last-player-ultimate-saison-2023-2024-8","double-limination", 0, 1511927],
    ["last-player-ultimate-saison-2023-2024-9-1","double-limination", 0, 1517274],
    ["last-player-ultimate-saison-2023-2024-10","double-limination", 0, 1521799],
    ["last-player-ultimate-saison-2023-2024-11","double-limination", 0, 1527370],
    ["last-player-ultimate-saison-2023-2024-12","double-limination", 0, 1536786],
    ["last-player-ultimate-saison-2023-2024-13","double-limination", 0, 1540648],
    ["last-player-ultimate-saison-2023-2024-14","double-limination", 0, 1543668],

    ["le-p-tit-p-nitencier","la-cours", 0, 1509090],
    ["le-p-tit-p-nitencier-2","la-cour", 0, 1514817],
    ["le-p-tit-p-nitencier-3","la-cour", 0, 1519415],
    ["le-p-tit-p-nitencier-4","la-cour", 0, 1524678],
    ["le-p-tit-p-nitencier-5","la-cour", 0, 1529957],
    ["le-p-tit-p-nitencier-6-1","la-cour", 0, 1534876],
    ["le-p-tit-p-nitencier-7","la-cour", 0, 1538360],
    ["le-p-tit-p-nitencier-8","la-cour", 0, 1541888],
    ["le-p-tit-p-nitencier-9-1","la-cour", 0, 1545373],


    // ... Ajoutez autant de tournois que nécessaire
];

let i =1;

for(i=1; i <= tournois.length-1; i++)
{
  
  console.log('avant le delai')

    // Ajoutez un délai de 4 secondes avant la prochaine itération
    await new Promise(resolve => setTimeout(resolve, 10000));


    console.log('après le delai')


    let LeTournoi = tournois[i];



    // Utilisez votre code ici
    LeTournoi[2] = await getEventId(LeTournoi[0], LeTournoi[1]);
    //let test = await getEventStandings(eventId, 1, 16); //Obtenir le top 16

    // READ
    //const users = await getUsers(); //BDD Locale
    //console.log('All users:', users);

    //let eventId2 = await getEventId('last-player-ultimate-saison-2023-2024-9-1', 'double-limination');

    //console.log(eventId2);    

    let tousLesPlayer =  JSON.stringify(await getEventStandings(LeTournoi[2], 1, 128))//Avoir tous les players
    //console.log("Liste des participants : " + tousLesPlayer); 


    //Début Calcul indiceStackNombreParticipant    

    let tousLesPlayerObject = JSON.parse(tousLesPlayer);
    let totalPlayer = tousLesPlayerObject.event.standings.nodes.length;
    
    console.log("Nombre de participants : " + totalPlayer);

    let indiceStackNombreParticipant = 0.005 * totalPlayer; 

    //console.log("indiceStackNombreParticipant : " + indiceStackNombreParticipant);

    //Fin Calcul indiceStackNombreParticipant



    //Début indiceStackWinRateParticipant
    async function processParticipants() {
      const promises = tousLesPlayerObject.event.standings.nodes.map(async (participant) => {
        let nomSimpleJoueur = participant.entrant.name;
    
        if (participant.entrant.name.includes('|')) {
          const nameParts = participant.entrant.name.split('|');
          nomSimpleJoueur = nameParts[1].substring(1);
        }
    
        return getUserByName(nomSimpleJoueur);
      });
    
      const joueurs = await Promise.all(promises);
    
      let testons = 0;

      for (const joueur of joueurs) {

        const currentJoueur = joueur[0];

        testons +=1;
    
        if (!currentJoueur || currentJoueur.length === 0) {
          let nomSimpleJoueur = tousLesPlayerObject.event.standings.nodes[testons-1].entrant.name;

          if (nomSimpleJoueur.includes('|')) {
            let nameParts = nomSimpleJoueur.split('|');
            nomSimpleJoueur = nameParts[1].substring(1);
          }

          await addPlayer(nomSimpleJoueur);
          console.log('Joueur ajouté à la BDD :', nomSimpleJoueur);
        } else {
          const winrate = currentJoueur.WinrateJoueur;
    
          if (winrate && winrate >= 0.45) {
            const thresholds = [45, 50, 55, 60, 65, 70, 75];
            const increments = [0.005, 0.01, 0.02, 0.03, 0.04, 0.06, 0.09];
    
            for (let i = 0; i < thresholds.length; i++) {
              if (winrate >= thresholds[i] && winrate < thresholds[i + 1]) {
                indiceStackNombreParticipant += increments[i];
                console.log('Nouvelle indice de stackité :', indiceStackNombreParticipant);
                break;
              }
            }
          }
        }


      }
    }
    // Appel de la fonction asynchrone
    processParticipants();
    //Fin indiceStackWinRateParticipant

    

    //const dd = await getUsers();
    //console.log('Résultat de getUsers :', dd);


    //let tessst = 'Supra';
    //const ddd = await getUserByName(tessst);
   //console.log('Résultat de getUsersByName :', ddd);

    

  tousLesPlayer =  JSON.stringify(await getEventStandings(LeTournoi[2], 1, 128))//Avoir tous les players
   //console.log("Liste des participants : " + tousLesPlayer); 


   //Début Calcul indiceStackNombreParticipant    

   tousLesPlayerObject = JSON.parse(tousLesPlayer);

    // Partie affichage du top 16
const top16 = tousLesPlayerObject.event.standings.nodes.slice(0, 16);

top16.forEach((participant, index) => {
  console.log(`Participant ${index + 1}:`);
  console.log(`Placement: ${participant.placement}`);
  console.log(`ID: ${participant.entrant.id}`);
  console.log(`Nom: ${participant.entrant.name}`);
  console.log('------------------------');
});

// Partie attribution des points pour le top 16
const promises = top16.map(participant => {
  let nomSimpleJoueur = participant.entrant.name;

  if (participant.entrant.name.includes('|')) {
    const nameParts = participant.entrant.name.split('|');
    nomSimpleJoueur = nameParts[1].substring(1);
  }

  return getUserByName(nomSimpleJoueur);
});

const joueurs = await Promise.all(promises);

for (let j = 0; j < joueurs.length; j++) {
  const joueur = joueurs[j][0];

  console.log('point du joueur au debut : ' + (joueur.PointsJoueur || 0));

  joueur.PointsJoueur = joueur.PointsJoueur || 0;

  const pointsArray = [50, 40, 30, 20, 10, 10, 5, 5, 2.5, 2.5, 2.5, 2.5, 1, 1, 1, 1];
  joueur.PointsJoueur += pointsArray[j] * indiceStackNombreParticipant;

  updatePoint(joueur.PointsJoueur, joueur.idJoueur);

}

//FIN PARTIE TOP 16 ATTRIBUTION POINTS


//Début partie point upset Factor



let tousLesSet =  await getEventSets(LeTournoi[2], 1, 600)//Avoir tous les players //sets non ??

let nombreDeSet = 0;

//console.log('Structure de tousLesSet :', tousLesSet.event.sets.nodes);

const phaseSeeds = await getPhaseSeeds(LeTournoi[3], 1, 128); //ID à défiler dans un tableau 
console.log('Phase Seeds:',  phaseSeeds);

// Vérifiez si les sets existent
if (tousLesSet && tousLesSet.event && tousLesSet.event.sets && tousLesSet.event.sets.nodes) {
  // Parcourez chaque set
  for (const [index, set] of tousLesSet.event.sets.nodes.entries()) {
    // Vérifiez si un participant est associé au premier et au deuxième slot du set
    if (set.slots && set.slots[0] && set.slots[1] && set.slots[0].entrant && set.slots[1].entrant) {
      // Obtenez les graines (seeds) des deux joueurs dans le set


      console.log('Joueur 1:',  set.slots[0].entrant.name);
      console.log('Joueur 2:',  set.slots[1].entrant.name);

      // Obtenez les informations sur le joueur 1
      let idJoueur1 = set.slots[0].entrant.id;
      let nomJoueur1 = set.slots[0].entrant.name;

      if (set.slots[0].entrant.name.includes('|')) {
        let nameParts1 = set.slots[0].entrant.name.split('|');
        nomJoueur1 = nameParts1[1].substring(1);
      }


      // Obtenez les informations sur le joueur 2
      let idJoueur2 = set.slots[1].entrant.id;
      let nomJoueur2 = set.slots[1].entrant.name;


      if (set.slots[1].entrant.name.includes('|')) {
        let nameParts2 = set.slots[1].entrant.name.split('|');
        nomJoueur2 = nameParts2[1].substring(1);
      }


      //Obtenir le seed du joueur dans le tournoi


      //console.log('phaseSeed phase :', phaseSeeds.phase.nodes)

      //console.log('Seed Num du joueur:',  phaseSeeds.phase.seeds.nodes[index].seedNum);
      //console.log('ID du joueur:',  phaseSeeds.phase.seeds.nodes[index].entrant.id);
      //console.log('Nom du joueur:',  phaseSeeds.phase.seeds.nodes[index].entrant.participants[0].gamerTag);

      
      let seedJoueur1 = 0;    
      let seedJoueur2 = 0;    


      phaseSeeds.phase.seeds.nodes.forEach(async (joueurSeed, indexx) => {

        if(nomJoueur1 == joueurSeed.entrant.participants[0].gamerTag)
        {
            seedJoueur1 = joueurSeed.entrant.participants[0].seedNum;  
        }
    });

    phaseSeeds.phase.seeds.nodes.forEach(async (joueurSeed, indexx) => {

        if(nomJoueur2 == joueurSeed.entrant.participants[0].gamerTag)
        {
            seedJoueur2 = joueurSeed.entrant.participants[0].seedNum;    
        }
    });


    if(seedJoueur1 != 0 && seedJoueur2 != 0)
    {
        //ça veut dire que l'on a les 2 joueurs du set, leur nom, id, seed

        console.log('Nom du joueur 1:',  nomJoueur1);
        console.log('Seed Num du joueur 1:', phaseSeeds.phase.seeds.nodes.find(joueurSeed => nomJoueur1 === joueurSeed.entrant.participants[0].gamerTag)?.seedNum || 0);

        seedJoueur1 = phaseSeeds.phase.seeds.nodes.find(joueurSeed => nomJoueur1 === joueurSeed.entrant.participants[0].gamerTag)?.seedNum || 0;

        console.log('Nom du joueur 2:',  nomJoueur2);
        console.log('Seed Num du joueur 2:', phaseSeeds.phase.seeds.nodes.find(joueurSeed => nomJoueur2 === joueurSeed.entrant.participants[0].gamerTag)?.seedNum || 0);

        seedJoueur2 = phaseSeeds.phase.seeds.nodes.find(joueurSeed => nomJoueur2 === joueurSeed.entrant.participants[0].gamerTag)?.seedNum || 0;

        //on a les seeds, mtn il faut vérifier les potentiels upsets factors


        // Vérifiez si les seeds sont aptes à entrer dans l'algorithme
        const isSeedApte = (seed) => {
            return (
            (seed >= 5 && seed <= 6) ||
            (seed >= 7 && seed <= 8) ||
            (seed >= 9 && seed <= 12) ||
            (seed >= 13 && seed <= 17) ||
            (seed >= 18 && seed <= 25) ||
            (seed >= 26 && seed <= 33) ||
            (seed >= 33 && seed <= 49) ||
            (seed >= 50 && seed <= 65) ||
            (seed >= 66 && seed <= 97) ||
            (seed >= 98 && seed <= 129)
            );
        };
        
        // Vérifiez si les seeds des deux joueurs sont aptes à entrer dans l'algorithme
        if (isSeedApte(seedJoueur1) && isSeedApte(seedJoueur2)) {
            // Les seeds sont aptes à entrer dans l'algorithme, faites ce que vous devez faire ici
        } else {
            // Les seeds ne sont pas aptes, faites ce que vous devez faire dans ce cas
            console.log('Risque dupset factor');


            if (seedJoueur1 > seedJoueur2) {
                console.log('seedJoueur1 ', nomJoueur1, 'est plus grand que seedJoueur2 ', nomJoueur2);


                let idDuSet1  = set.id;

                console.log('Id du set : ', idDuSet1);

                let ScoreDuSet1 = await getSetInfo(idDuSet1)

                if(ScoreDuSet1 != undefined)
                {

                  //en gros si c'est J1 qui gagne y'a upset
                  console.log('Score du set : ', ScoreDuSet1.set.slots[0].standing.placement); //si lui est égal a 1, upset 
                  console.log('Score du set 2: ', ScoreDuSet1.set.slots[1].standing.placement);//Si lui est égal à 2, upset

                  if(ScoreDuSet1.set.slots[0].standing.placement == 1)
                  {
                      console.log('UPSET DE ZINZIN DE ',nomJoueur1, 'SUR ', nomJoueur2);

                      //calculer la taille de l'upset factor


                      

                      function calculateUpsetFactor(seed1, seed2) {
                        const upsetScale = [
                          1,
                          2,
                          3,
                          4,
                          [5, 6],
                          [7, 8],
                          [9, 12],
                          [13, 17],
                          [18, 25],
                          [26, 33],
                          [33, 49],
                          [50, 65],
                          [66, 97],
                          [98, 129],
                        ];
                      
                        // Calcule la différence entre les deux seeds
                        const seedDifference = Math.abs(seed1 - seed2);
                      
                        // Parcoure l'échelle d'upset factor pour déterminer le niveau d'upset factor
                        for (let i = 0; i < upsetScale.length; i++) {
                          const scale = upsetScale[i];
                      
                          if (Array.isArray(scale)) {
                            // Si c'est un tableau, vérifiez si la différence est dans l'intervalle
                            const [min, max] = scale;
                            if (seedDifference >= min && seedDifference <= max) {
                              return i + 1; // Niveau d'upset factor
                            }
                          } else {
                            // Si c'est un nombre simple, vérifiez si la différence est égale à ce nombre
                            if (seedDifference === scale) {
                              return i + 1; // Niveau d'upset factor
                            }
                          }
                        }
                      
                        return -1; // Aucun upset factor dans l'échelle spécifiée
                      }



                      let upsetFactor = calculateUpsetFactor(seedJoueur1, seedJoueur2);

                      upsetFactor -= 1;

                      console.log('Upset Factor:', upsetFactor); // Devrait afficher 3 dans cet exemple



                      //on donne les point à joueur 1 et on les retires à joueur 2

                      let pointUpsetFactor = 3*upsetFactor;




                      let joueurBddGagnant = await getUserByName(nomJoueur1);

                      let joueurBddPerdant = await getUserByName(nomJoueur2);


                      let pointRankingGagnant = joueurBddGagnant[0].PointsJoueur += pointUpsetFactor;
                      let pointRankingPerdant = joueurBddPerdant[0].PointsJoueur -= pointUpsetFactor


                      await updatePoint(pointRankingGagnant, joueurBddGagnant.idJoueur);
                      await updatePoint(pointRankingPerdant, joueurBddPerdant.idJoueur);



                    
                  }
                }






              } else if (seedJoueur1 < seedJoueur2) {
                //console.log('seedJoueur1 ', nomJoueur1, 'est plus petit que seedJoueur2', nomJoueur2);


                let idDuSet2  = set.id;

                console.log('Id du set : ', idDuSet2);

                let ScoreDuSet2 = await getSetInfo(idDuSet2)


                if(ScoreDuSet2 != undefined)
                {
                  
             
                

                  console.log('Score du set marge ?: ', ScoreDuSet2.set.slots[0].standing.placement); //si lui est égal a 2, upset 
                  console.log('Score du set 2: ', ScoreDuSet2.set.slots[1].standing.placement);//Si lui est égal à 1, upset


                  if(ScoreDuSet2.set.slots[0].standing.placement == 2)
                  {
                      console.log('UPSET DE ZINZIN DE ',nomJoueur2, 'SUR ', nomJoueur1);

                      //Obtenir la taille de l'upset factor


                      function calculateUpsetFactor(seed1, seed2) {
                        const upsetScale = [
                          1,
                          2,
                          3,
                          4,
                          [5, 6],
                          [7, 8],
                          [9, 12],
                          [13, 17],
                          [18, 25],
                          [26, 33],
                          [33, 49],
                          [50, 65],
                          [66, 97],
                          [98, 129],
                        ];
                      
                        // Calcule la différence entre les deux seeds
                        const seedDifference = Math.abs(seed1 - seed2);
                      
                        // Parcoure l'échelle d'upset factor pour déterminer le niveau d'upset factor
                        for (let i = 0; i < upsetScale.length; i++) {
                          const scale = upsetScale[i];
                      
                          if (Array.isArray(scale)) {
                            // Si c'est un tableau, vérifiez si la différence est dans l'intervalle
                            const [min, max] = scale;
                            if (seedDifference >= min && seedDifference <= max) {
                              return i + 1; // Niveau d'upset factor
                            }
                          } else {
                            // Si c'est un nombre simple, vérifiez si la différence est égale à ce nombre
                            if (seedDifference === scale) {
                              return i + 1; // Niveau d'upset factor
                            }
                          }
                        }
                      
                        return -1; // Aucun upset factor dans l'échelle spécifiée
                      }



                      let upsetFactor = calculateUpsetFactor(seedJoueur2, seedJoueur1);

                      upsetFactor -= 1;

                      console.log('Upset Factor:', upsetFactor-1); // Devrait afficher 3 dans cet exemple



                      //on donne les point à joueur 2 et on les retires à joueur 1


                      let pointUpsetFactor = 3*upsetFactor;




                      let joueurBddGagnant = await getUserByName(nomJoueur2);

                      let joueurBddPerdant = await getUserByName(nomJoueur1);



                      let pointRankingGagnant = joueurBddGagnant[0].PointsJoueur += pointUpsetFactor;
                      let pointRankingPerdant = joueurBddPerdant[0].PointsJoueur -= pointUpsetFactor


                      await updatePoint(pointRankingGagnant, joueurBddGagnant.idJoueur);
                      await updatePoint(pointRankingPerdant, joueurBddPerdant.idJoueur);



                      let debug= 0;
                    }

                  }

              } 



        }

    }
      
    } 

    nombreDeSet += 1;

  };
}




}

console.log("algo terminé");
}


main();  // Appelez la fonction principale

